using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_RPPOON
{
    class BoxITerator : IAbstractIterator
    {
        private Box box;
        private int Position;
        public BoxITerator(Box box)
        {
            this.box = box;
            this.Position = 0;
        }
        public bool IsDone
        {
            get
            {
                return this.Position >= box.Count;
            }
        }

        public Product Current
        {
            get
            {
                return this.box[this.Position];
            }
        }

        public Product First()
        {
            return this.box[0];
        }

        public Product Next()
        {
            this.Position++;
            if (this.IsDone)
            {
                return null;
            }
            else
            {
                return this.box[this.Position];
            }
        }
    }

}

