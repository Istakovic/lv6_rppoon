using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
        // 1 Zadatak
        
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("1st note", "MOGU JAA TOOO"));
            notebook.AddNote(new Note("2nd note", "Danas je cetvrtak"));
            notebook.AddNote(new Note("3rd note", "Proci cu RPPOON"));
            IAbstractIterator noteIterator = notebook.GetIterator();
            while (noteIterator.IsDone != true)
            {
                noteIterator.Current.Show();
                noteIterator.Next();
            }
        // 2 Zadatak
        
            Box box = new Box();
            box.AddProduct(new Product("Iphone", 10));
            box.AddProduct(new Product("Samsung", 100));
            IAbstractIterator boxITerator = box.GetIterator();
            boxITerator = box.GetIterator();
            while (boxITerator.IsDone != true)
            {
                Console.WriteLine(boxITerator.Current.ToString());
                boxITerator.Next();
            }
        }
    }
}
